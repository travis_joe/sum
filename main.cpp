#include <iostream>

using namespace std;

int main() {
    const int numbersToAdd = 10; // How many numbers will be used as input?

    int positiveNumberSum = 0; // Sum of all positive numbers
    int negativeNumberSum = 0; // Sum of all negative numbers
    int totalSum = 0; // Total sum of all numbers

    int currentNumber = 1;  // current position in the loop
    while (currentNumber <= numbersToAdd) {
        int number = 0;
        cout << "Enter number " << currentNumber << ": ";
        cin >> number;

        if(number < 0) {
            negativeNumberSum += number;
        } else if(number > 0) {
            positiveNumberSum += number;
        }

        totalSum += number;

        currentNumber++;
    }

    cout << "Total sum: " << totalSum << "\n";
    cout << "Positive number sum: " << positiveNumberSum << "\n";
    cout << "Negative number sum: " << negativeNumberSum << "\n";

    return 0;
}